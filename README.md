# gnunet RPMs

Tested with gnunet@bbcc29382.

```sh
rpmdev-setuptree
tar -cvf ~/rpmbuild/SOURCES/gnunet.tar ~/gnunet
cp gnunet.spec ~/rpm/SPECS
cd ~/rpmbuild
rpmbuild -bs SPECS/gnunet.spec
# versions vary depending on the .spec
mock -r fedora-29-x86_64 SRPMS/gnunet-0.11.0pre66-1.src.rpm
ls -l /var/lib/mock/fedora-29-x86_64/result/gnunet-0.11.0pre66-1.x86_64.rpm
```
